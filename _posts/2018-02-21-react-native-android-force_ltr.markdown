---
layout: post
title:  "جلوگیری از RTL شدن اندروید"
date:   2018-02-16 17:54:00 +0330
categories: react-native
tags: android ltr
---
در فایل AndroidManifest.xml این را داخل خصوصیت های application اضافه می کنیم:

    
```
android:supportsRtl="false"
```

در فایل MainApplication.java در  بالا این را اضافه می کنیم:

```java
import com.facebook.react.modules.i18nmanager.I18nUtil;
```

و محتوای تابع onCreate به این صورت تغییر می دهیم:

```java
@Override
public void onCreate() {
    super.onCreate();
    
    // FORCE LTR
    I18nUtil sharedI18nUtilInstance = I18nUtil.getInstance();
    sharedI18nUtilInstance.allowRTL(getApplicationContext(), false);
    
    SoLoader.init(this, /* native exopackage */ false);
}
```