---
layout: post
title:  "بهبود عملکرد Watchman"
date:   2018-02-16 17:54:00 +0330
categories: react-native
tags: watchman
---
در مسیر فایل زیر را ساخته:

```
/etc/watchman.json  
```

و داخل آن این خطوط را وارد می کنیم:

```
{
  "root_files": [".watchmanconfig"],
  "enforce_root_files": true
}
```

خط اول مشخص می کند که فقط فولدر هایی که فایل watchmanconfig داشته باشند را جستجو کند و مجبور می کند که این قابلیت حتما انجام شود.