---
layout: post
title:  "افزودن Font"
date:   2018-02-16 17:54:00 +0330
categories: react-native
tags: font
---
1. ساخت پوشه فونت ها

    مثلاً: 
    
    ```
    ./assets/fonts
    ```

2. افزودن خط زیر در فایل package.json

    ```
    "rnpm": {
        "assets": ["./path to your fonts"]
    }
    ```

3. اجرای دستور زیر در ترمینال
    ```
    react-native link
    ```


براساس آموزش [Medium](https://medium.com/@danielskripnik/how-to-add-and-remove-custom-fonts-in-react-native-b2830084b0e4)