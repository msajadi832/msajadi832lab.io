---
layout: post
title:  "My Favorite Apps"
date:   2018-04-26 10:54:00 +0330
categories: linux
tags: app program
---

# Apps
  * Clementine
  * Toggl
  * WebStorm
  * PhpStorm
  * GIT
  * Android Studio
  * MPV
  * Telegram
  * Persepolis
  * VMware
  * GIMP
  * Postman
  * Google Chrome
  * ReText
  * Thunderbird
  * gThumb
  * GParted
  * Kazam
  * Tilda
  * StarCal 3
  * Nethogs
  * Tor
  * Obfs4proxy
  * MailNag
  * gnome-tweak-tool
  * NginX
  * mariadb
  * php 7
  * phpmyadmin
  * watchman
  * Composer
  * Node JS
  * React Native cli
  * Golden Dict
  * Yarn
  * NPM
  * jdk 8
  * network-manager-openvpn-gnome
  * breeze-cursor-theme
  * Flat-Remix (from git folder)
  * create_ap (from git folder)
  * 

# Gnome Extensions
  * Dash to Dock
  * Desk Changer
  * Gsconnect
  * Lock keys
  * Mailnag
  * Start overlay in application menu: اگر لازم شد نصب شود
  * Transparent OSD
  * Unite
  * System-monitor
