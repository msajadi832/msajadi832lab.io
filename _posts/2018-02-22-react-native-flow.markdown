---
layout: post
title:  "Flow"
date:   2018-02-22 11:54:00 +0330
categories: react-native
tags: flow
---
ابتدا در پروژه در فایل ```.flowconfig``` انتهای فایل این عبارت ها وجود دارد:

```
[version]
^0.63.0
```

بر اساس نسخه ای که در این فایل مشخص شده، باید flow را نصب کنیم:

```
yarn add flow-bin@0.63.0 --dev
```

برای اجرا flow از طریق ترمینال نیز، در فایل ```package.json```  در قسمت scripts این خط را می افزاییم:

```
"scripts": {
    "start": "node node_modules/react-native/local-cli/cli.js start",
    "test": "jest",
    "flow": "flow"
},
```

حال با استفاده از دستور زیر در ترمینال می توان خطاهای موجود را دید:

```
npm run flow
```


منبع:
[Getting Started with React Native and Flow](https://medium.com/react-native-training/getting-started-with-react-native-and-flow-d40f55746809)