---
layout: post
title:  "فقط حالت عمودی در اندروید"
date:   2018-02-16 18:54:00 +0330
categories: react-native
tags: android vertical
---
در فایل AndroidManifest.xml در activity:

از خصوصیت android:configChanges آن عبارت orientation را حذف می کنیم.

و خط زیر را به خصوصیت های activity می افزاییم:

```
android:screenOrientation="portrait"
```