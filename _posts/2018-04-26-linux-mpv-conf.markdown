---
layout: post
title:  "MPV Config"
date:   2018-04-26 11:56:00 +0330
categories: linux
tags: mpv video config movie
---
فایل زیر را اگر وجود ندارد بسازید:

```/home/morteza/.config/mpv/mpv.conf```

و عبارات زیر را داخل آن وارد کنید:

```
--hwdec=vaapi
--volume='100'
--sub-text-font='B Yekan'
--sub-text-color='#ffff00'
--sub-scale-with-window='yes'
--secondary-sid='auto'
--geometry='0:0'
--autofit-larger
--sub-codepage=utf8:cp1256
--save-position-on-quit

```