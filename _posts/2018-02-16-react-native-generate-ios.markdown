---
layout: post
title:  "خروجی Release برای IOS"
date:   2018-02-16 13:35:00 +0330
categories: react-native
tags: ios release
---
 1. تنظیم ورژن و بیلد
 
    در Xcode -> Project navigator مورد root را انتخاب کنید و Target درست را انتخاب کنید و ورژن را تنظیم کنید.

 2. ساخت ورژن جدید در Itunes
 
    اگر می خواهید رو Appstore بگذارید، در آدرس [https://itunesconnect.apple.com](https://itunesconnect.apple.com) یک نسخه جدید ایجاد کنید.

 3. فعال کردن App Transport Security
 
    در فایل Info.plist از NSExceptionDomains آیتم localhost حذف شود.

 4. تنظیم release scheme

    از منوها، Product -> Scheme -> Edit Scheme را انتخاب کنید. از نوار سمت چپ گزینه Run را انتخاب کنید و Build Configuration را روی Release بگذارید.

5. از منوها، Product -> Build

6. در ابزار های بالا، دستگاه را روی Generic IOS Device قرار دهید.

7. از منوها، Product -> Archive


* برای گرفتن Screen Shot خوب برای Appstore از دستگاه Iphone 6s plus در شبیه ساز استفاده شود.
* برای ساخت Icon می توان از سایت [http://makeappicon.com](http://makeappicon.com) استفاده نمود.