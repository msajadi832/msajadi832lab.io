---
layout: post
title:  "Gradle Proxy"
date:   2018-04-26 10:54:00 +0330
categories: react-native
tags: gradle proxy
---
به مسیر زیر بروید:

```android/gradle.properties```

و عبارات زیر را به آخر فایل بیافزایید:

```
systemProp.http.proxyHost=fod.backtory.com
systemProp.http.proxyPort=8118
systemProp.http.nonProxyHosts=*.jitpack.io, *.maven.org
systemProp.https.proxyHost=fod.backtory.com
systemProp.https.proxyPort=8118
systemProp.https.nonProxyHosts=*.jitpack.io, *.maven.org

```